# Authentication using a per-request script

Fork this project into a namespace with an Ultimate license for this example to work.

This is an example of API authentication that integrates information from each request
to generate an authorization token. This method can be used in combination with the overrides system.

Only use this method if the authorization token uses portions of the request such as the URL, headers, and request body. If the authorization doesn't require portions of the request, use overrides instead.

This demo project includes several examples of using per-request scripts for various types of authentication needs.

- Must use C# to write per-request scripts
- Use 3rd party .NET libraries? Yes, [see the references section.](#reference)

## Testing per-request scripts

The API Security Testing container image provides a mode of operation created specifically to assist in testing overrides and per-request scripts. The following command line can be used locally to test scripts. It will run API Security Testing in passive mode (no active testing) and with reporting disabled. It will attempt to call each API operation allowing verification that authentication is working.

```shell
docker run --rm -v "$(pwd):$(pwd)" -w "$(pwd)" -u $UID \
    --env-file "ENV_FILE" \
    registry.gitlab.com/security-products/api-security:5 \
    /peach/analyzer-api-security-test
```

- `ENV_FILE` is an environment file in the format needed by docker (`KEY=VALUE`, one per line). This file must contain a valid API Security Testing configuration.

## Examples

### Example: Hash API key with URL and body of the request

In this example, an API key is combined with the request URL and request body to generate a hash that is
provided via the `Authorization` header.

- Script: `per_request_hash_body.cs`
- Target: `target_hash_body`
- Testing: `per_request_hash_body.test.env`

_Example GitLab CICD YAML:_

```yaml
api_security:
    services:
        - name: $TARGET_IMAGE_HASH_BODY
          alias: target
    variables:
        APISEC_OPENAPI: rest_target_openapi.json
        APISEC_TARGET_URL: http://target:7777
        APISEC_PER_REQUEST_SCRIPT: per_request_hash_body.cs
        TEST_API_KEY: b5638ae7-6e77-4585-b035-7d9de2e3f6b3
```

#### Testing

1. Start `target_hash_body` by running `python rest_target.py`
1. Update `per_request_hash_body.test.env` with the correct IP address for the target.
1. Run the test:

   ```shell
   docker run --rm -v "$(pwd):$(pwd)" -w "$(pwd)" -u $UID \
       --env-file "per_request_hash_body.test.env" \
       registry.gitlab.com/security-products/api-security:5 \
       /peach/analyzer-api-security-test
   ```

### Example: Signed JWT token using RS256

In this example, a JWT token is created using the `RS256` algorithm. The token
is signed with an RSA encryption key.

- Script: `per_request_jwt_sign.cs`
- Target: `target_jwt_sign`
- Testing: `per_request_jwt_sign.test.env`

_Example GitLab CICD YAML:_

```yaml
api_security:
    services:
        - name: $TARGET_IMAGE_JWT_SIGN
          alias: target
    variables:
        APISEC_OPENAPI: rest_target_openapi.json
        APISEC_TARGET_URL: http://target:7777
        APISEC_PER_REQUEST_SCRIPT: per_request_jwt_sign.cs
        TEST_RSA_KEY: $CI_PROJECT_DIR/target_jwt_sign/rsa.key
```

#### Testing

1. Start `target_jwt_sign` by running `python rest_target.py`
1. Update `per_request_jwt_sign.test.env` with the correct IP address for the target.
1. Run the test:

   ```shell
   docker run --rm -v "$(pwd):$(pwd)" -w "$(pwd)" -u $UID \
       --env-file "per_request_jwt_sign.test.env" \
       registry.gitlab.com/security-products/api-security:5 \
       /peach/analyzer-api-security-test
   ```

## Reference

The per-request scripts are .NET C# source files that define a class that implements the `IPerRequestScript`
interface. These are regular C# source files with one exception, support for referencing other .NET assemblies.

### Referencing .NET assemblies

Adding references to .NET assemblies (libraries/packages) uses a special comment type in the format of `#r 'PATH/TO/ASSEMBLY.DLL'` which can be added at the top of a script file. This will be found and the referenced assemblies loaded into the script context.

```csharp
//#r 'jose-jwt'
//#r 'System.Security.Cryptography'
//#r 'MyCustomPackage.dll'

using xyz;
using zyx;

class MyPerRequestScript : IPerRequestScript
{
    // ...
}
```

#### Default references

The following references are automatically added to the script context:

- `netstandard`
- `Peach.Web`
- `Peach.Web.Network`
- `Microsoft.Extensions.Logging.Abstractions`
- `System.Collections`
- `System.Memory`
- `System.Private.CoreLib`
- `System.Runtime`
- `System.Security.Cryptography`
- `System.Security.Cryptography.Algorithms`
- `System.Security.Cryptography.Cng`
- `System.Security.Cryptography.Primitives`

### Using both an overrides script and per-request script

Often you might want to use both an overrides script and also an auth script.
The overrides script would perform actions not needed on each request such as
getting a key that is mixed in with the request data to generate the actual
auth token.

To do this, have the overrides script set the key in the final location for the
auth token, for example, the `Authorization`` header. The auth script can then
read this value from the request and update it after mixing in data from the
request itself.

In the example script from this project we could do this by changing how the
`api_key` is set to get the value from the request headers:

```python
api_key = req.headers['Authorization']
```

The override script would set the API key in the `Authorization` header making
it available for use by the auth script.

### Reference: IPerRequest interface

```csharp
public interface IPerRequestScript
{
    void OnRequest(OnRequestParameters param);
}

public class OnRequestParameters
{
    public Request Request { get; set; }
    public byte[] RequestBody { get; set; }
}
```

### Reference: Request class

```csharp
public class Request
{
    public bool IsHttps => Uri.Scheme == Uri.UriSchemeHttps;
    public string RequestLine { get; set; }

    /// <summary>
    /// Request Method
    /// </summary>
    public string Method { get; set; }

    /// <summary>
    /// Request HTTP Uri
    /// </summary>
    public Uri Uri { get; set; }

    /// <summary>
    /// Request Http Version
    /// </summary>
    public Version HttpVersion { get; set; } = new Version(1, 1);

    /// <summary>
    /// Ignore me, internal use only
    /// </summary>
    public MemoryStream RawInput { get; set; }
    /// <summary>
    /// Ignore me, internal use only
    /// </summary>
    public MemoryStream RawOutput { get; set; }

    /// <summary>
    /// Request Http hostname
    /// </summary>
    public string Host { get; set; }

    /// <summary>
    /// Request content encoding
    /// </summary>
    public string ContentEncoding { get; }

    /// <summary>
    /// Request content-length
    /// </summary>
    public long ContentLength { get; set; }

    /// <summary>
    /// Request content-type
    /// </summary>
    public string ContentType { get; set; }

    /// <summary>
    /// Is request body send as chunked bytes
    /// </summary>
    public bool IsChunked { get; }

    /// <summary>
    /// Does this request have an 'Expect: 100-continue' header?
    /// </summary>
    public bool ExpectContinue { get; }

    /// <summary>
    /// Request Url
    /// </summary>
    public string Url { get; }

    /// <summary>
    /// Request body as byte array
    /// </summary>
    public byte[] Body { get; set; }

    /// <summary>
    /// Unique Request header collection
    /// </summary>
    public Dictionary<string, HttpHeader> Headers { get; set; }

    /// <summary>
    /// Non Unique headers
    /// </summary>
    public Dictionary<string, List<HttpHeader>> NonUniqueHeaders { get; set; }

    public Request() {}

    public void AddViaHeader(string name) {}
    public void RewriteUri(System.Uri baseUrl) {}

    /// <summary>
    /// Read request body content as bytes[] for current session
    /// </summary>
    public async Task<byte[]> ReadBody(ICustomBinaryReader reader, CancellationToken token) {}

    public override string ToString() {}

    public static async Task<Request> Parse(
        ICustomBinaryReader reader,
        CancellationToken token,
        Request connect = null,
        string endPoint = null,
        bool parseBody = false,
        bool fixHeaders = true,
        bool hideSecrets = false) {}

    public async Task WriteHeaders(Stream stream, bool absolute) {}

    public async Task ParseHeaders(ICustomBinaryReader reader, CancellationToken token) {}

    public void AddHeader(HttpHeader header) {}

    public void HideSecrets() {}

    /// <summary>
    /// Gets any cookie headers present in the request. Each <c>Cookie</c> header is
    /// represented as one <see cref="CookieHeaderValue"/> instance. A <see cref="CookieHeaderValue"/>
    /// contains information about the domain, path, and other cookie information as well as one or
    /// more <see cref="CookieState"/> instances. Each <see cref="CookieState"/> instance contains
    /// a cookie name and whatever cookie state is associate with that name. The state is in the form of a
    /// <see cref="System.Collections.Specialized.NameValueCollection"/> which on the wire is encoded as HTML Form URL-encoded data.
    /// This representation allows for multiple related "cookies" to be carried within the same
    /// <c>Cookie</c> header while still providing separation between each cookie state. A sample
    /// <c>Cookie</c> header is shown below. In this example, there are two <see cref="CookieState"/>
    /// with names <c>stateA</c> and <c>stateB</c> respectively. Further, each cookie state contains two name/value
    /// pairs (name1/value1 and name2/value2) and (name3/value3 and name4/value4).
    /// <code>
    /// Cookie: stateA=name1=value1&amp;name2=value2; stateB=name3=value3&amp;name4=value4; domain=domain1; path=path1;
    /// </code>
    /// </summary>
    /// <returns>A collection of <see cref="CookieHeaderValue"/> instances.</returns>
    public Collection<CookieHeaderValue> GetCookies() {}

    public static Collection<CookieHeaderValue> GetCookies(IEnumerable<string> cookieHeaders) {}
}
```

### Reference: HttpHeader class

```csharp
public class HttpHeader
{
    public HttpHeader(string name, string value)
    {
        if (string.IsNullOrEmpty(name))
        {
            throw new Exception("Name cannot be null");
        }

        Name = name.Trim();
        Value = value.Trim();
    }

    public string Name { get; set; }
    public string Value { get; set; }

    /// <summary>
    /// Returns header as a valid header string
    /// </summary>
    /// <returns></returns>
    public string ToString(int maxValueLength = -1)
    {
        return maxValueLength == -1 ?
            $"{Name}: {Value}" :
            $"{Name}: {(Value.Length < maxValueLength ? Value : (Value.Substring(0, maxValueLength) + "[...Truncated]"))}";
    }
}
```


### Reference: Uri class

```csharp
public enum UriPartial
{
    Scheme = 0,
    Authority = 1,
    Path = 2,
}

public class Uri
{
    // Fields

    public static readonly string SchemeDelimiter = "://";
    public static readonly string UriSchemeFile = "file";
    public static readonly string UriSchemeFtp = "ftp";
    public static readonly string UriSchemeGopher = "gopher";
    public static readonly string UriSchemeHttp = "http";
    public static readonly string UriSchemeHttps = "https";
    public static readonly string UriSchemeMailto = "mailto";
    public static readonly string UriSchemeNews = "news";
    public static readonly string UriSchemeNntp = "nntp";

    // Public

    public string OriginalString { get { return source; } }
    public bool IsHttps => Scheme == UriSchemeHttps;

    // Constructors

    public Uri(string uriString) : this(uriString, false) {}
    public Uri(string uriString, bool dontEscape) {}
    public Uri(string uriString, bool dontEscape, bool reduce) {}
    public Uri(Uri baseUri, string relativeUri) {}
    public Uri(Uri baseUri, string relativeUri, bool dontEscape) {}

    // Properties

    public string AbsolutePath {get;}
    public string AbsoluteUri {get;}
    public string Authority {get;}
    public string BaseUri {get; }
    public string Fragment { get; }
    public string Host { get; }
    public bool IsDefaultPort { get; }
    public bool IsFile {get;}
    public bool IsLoopback { get; }
    public bool IsUnc { get; }
    public string LocalPath {get;}
    public string PathAndQuery {get;}
    public int Port {get;}
    public string Query { get; }
    public string Scheme { get; }
    public string[] Segments { get; }
    public bool UserEscaped { get; }
    public string UserInfo { get; }

    // Methods

    public static bool CheckSchemeName(string schemeName) {}
    public override bool Equals(object comparant) {}
    public override int GetHashCode() {}
    public string GetLeftPart(UriPartial part) {}
    public static int FromHex(char digit) {}
    public static string HexEscape(char character) {}
    public static char HexUnescape(string pattern, ref int index) {}
    public static bool IsHexDigit(char digit) {}
    public static bool IsHexEncoding(string pattern, int index) {}
    public string MakeRelative(Uri toUri) {}
    public override string ToString() {}
}
```
