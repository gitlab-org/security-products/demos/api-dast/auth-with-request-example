// Add a reference to the jose-jwt package. This
// package comes pre-installed with API Security.
// https://github.com/dvsekhvalnov/jose-jwt
//#r 'jose-jwt'
using Jose;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using Microsoft.Extensions.Logging;
using Peach.Web.Core.Interfaces;
using Peach.Web.Network.Models;
using Peach.Web.Network.Http;

class JwtSignPerRequestScript : IPerRequestScript
{
	private readonly ILogger _logger;
	private readonly RSA _rsaPrivateKey;

	public JwtSignPerRequestScript(ILogger logger)
	{
		// Perform one time initialization. This is a good place
		// to load environment variables, public/private keys, certificates, etc.
		// The things that don't need to be done on each request.

		_logger = logger;

		var rsaKeyFile = Environment.GetEnvironmentVariable("TEST_RSA_KEY");
		if (rsaKeyFile == null || rsaKeyFile == string.Empty)
		{
			_logger.LogError("Error, TEST_RSA_KEY not set.");
			throw new Exception("Error, TEST_RSA_KEY not set.");
		}

		// Load the RSA private key that will be used
		// to sign the payload.
		_rsaPrivateKey = RSA.Create();
		_rsaPrivateKey.ImportFromPem(File.ReadAllText(rsaKeyFile));
	}

	public void OnRequest(OnRequestParameters param)
	{
		// Compute the signed JWT token using RS256 algorithm

		var req = param.Request;
		var body = param.RequestBody;
		var requestUrl = req.Uri;
		var port = string.Empty;

		// Normalize the URL
		if (!requestUrl.IsDefaultPort)
			port = $":{requestUrl.Port.ToString()}";
		var url = $"{requestUrl.Scheme}://{requestUrl.Host}{port}{requestUrl.PathAndQuery}";

		// Convert URL string to byte array
		var urlBytes = Encoding.UTF8.GetBytes(url);

		// Compute the SHA256 hash of url + body + http-method
		using var hash = IncrementalHash.CreateHash(HashAlgorithmName.SHA256);
		hash.AppendData(urlBytes);
		if (body != null && body.Length > 0)
			hash.AppendData(body);
		hash.AppendData(Encoding.ASCII.GetBytes(req.Method));
		var hashBytes = hash.GetHashAndReset();
		// Convert hash from byte array to hex string
		var edts = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
		_logger.LogDebug($"edts: {edts}");

		var payload = new Dictionary<string, object>()
		{
			{"edts", edts},
			{"v", "1"},
			{"exp", DateTime.UtcNow + TimeSpan.FromSeconds(120) },
			{"iat", DateTime.UtcNow },
			{"ehts", "uri;body;http-method" },
			{"jti", Guid.NewGuid() },
		};

		// Compute the signed token
		var token = Jose.JWT.Encode(payload, _rsaPrivateKey, JwsAlgorithm.RS256);
		_logger.LogDebug($"token: {token}");

		// Set the authorization header
		req.Headers["authorization"] = new HttpHeader("Authorization", $"Token {token}");
	}
}
