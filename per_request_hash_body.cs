using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using Microsoft.Extensions.Logging;
using Peach.Web.Core.Interfaces;
using Peach.Web.Network.Models;
using Peach.Web.Network.Http;

class HashBodyPerRequestScript : IPerRequestScript
{
	private readonly ILogger _logger;
	private readonly byte[] _apiKey;

	public HashBodyPerRequestScript(ILogger logger)
	{
		// Perform one time initialization. This is a good place
        // to load environment variables, public/private keys, certificates, etc.
        // The things that don't need to be done on each request.

		_logger = logger;
		var apiKey = Environment.GetEnvironmentVariable("TEST_API_KEY");

		if (apiKey == null || apiKey == string.Empty)
		{
			_logger.LogError("Error, TEST_API_KEY not set.");
			throw new Exception("Error, TEST_API_KEY not set.");
		}

        // Convert a string to a byte array for later use
		_apiKey = Encoding.UTF8.GetBytes(apiKey);
	}

	public void OnRequest(OnRequestParameters param)
	{
        // Compute the authorization token
        // by hashing: URL, body, and API key.
        // The hashing algorithm is SHA256

        var req = param.Request;
        var body = param.RequestBody;
		var requestUrl = req.Uri;
		var port = string.Empty;

        // Normalize the URL
		if(!requestUrl.IsDefaultPort)
            port = $":{requestUrl.Port.ToString()}";
		var url = $"{requestUrl.Scheme}://{requestUrl.Host}{port}{requestUrl.PathAndQuery}";
		_logger.LogDebug($"url: {url}");

        // Convert URL string to byte array
		var urlBytes = Encoding.UTF8.GetBytes(url);

        // Compute the SHA256 hash of url + body + api_key
		using var hash = IncrementalHash.CreateHash(HashAlgorithmName.SHA256);
        hash.AppendData(urlBytes);
        if(body != null && body.Length > 0)
            hash.AppendData(body);
        hash.AppendData(_apiKey);
        var hashBytes = hash.GetHashAndReset();

        // Convert hash from byte array to hex string
		var hashToken = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();

        // Set the authorization header
		req.Headers["authorization"] = new HttpHeader("Authorization", $"Token {hashToken}");
	}
}
